﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Webapi_test.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //Console.WriteLine("Enter number for side 1 ");
            //var side1 = Console.ReadLine();
            //Console.WriteLine("Enter number for side 2 ");
            //var side2 = Console.ReadLine();
            //Console.WriteLine("Enter number for side 3");
            //var side3 = Console.ReadLine();
           // var ttype = GetTriangleType(Convert.ToInt32(side1), Convert.ToInt32(side2), Convert.ToInt32(side3));
            //Console.WriteLine("Type of triangle is " + ttype);
            //Console.ReadLine();
            return View(); 

        }
        [Route("api/{Home}/GetTriangleType")]
        public string GetTriangleType(int side1, int side2, int side3)
        {
            var values = new int[3] { side1, side2, side3 };
            //ViewData["side1"] = side1;
            //ViewData["side2"] = side2;
            //ViewData["side3"] = side3;
            string result=string.Empty ;
            if (side1 > 0 && side2 > 0 && side3 > 0)
                switch (values.Distinct().Count())
                {
                    case 1:
                        return  result= "Equilateral";
                    case 2:
                        return  result= "Isosceles";
                    case 3:
                        return result= "Scalene";
                    default:
                        return result ;
                }
            else
                // return TriangleType.Error;
                return result ="Error returning type of triangle";
        }
        public string StringReverse(string input)
        {


            if (input == null) throw new ArgumentNullException(nameof(input));
            char[] punctuation = new char[] { ' ', ',', '.', ':', '\t', '?', '!', '@', '#' };
            char[] inputAsChars = input.ToCharArray();
            var result = string.Empty;

            var temp = "";
            foreach (var c in inputAsChars)
            {
                if (punctuation.Contains(c))
                {

                    char[] tx = temp.ToCharArray();
                    Array.Reverse(tx);
                    temp = new string(tx);
                    result += temp + c;
                    temp = "";
                }
                else
                    temp += c;
            }
            return result;
        }
        public int Fibbonacci(int num)
        {
            int a = 0;
            int b = 1;
            int result = 1;

           

            for (uint i = 0; i < num; i++)
            {
                result = b + a;
                a = b;
                b = result;
            }
            return result;
            
            


        }

            
        
    }
}
