﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Protocols.WSTrust;
using System.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.Text;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin.Security;
using SecurityAlgorithms = System.IdentityModel.Tokens.SecurityAlgorithms;
using SecurityToken = System.IdentityModel.Tokens.SecurityToken;
using SecurityTokenDescriptor = System.IdentityModel.Tokens.SecurityTokenDescriptor;
using SigningCredentials = System.IdentityModel.Tokens.SigningCredentials;

namespace Webapi_test.Controllers
{
    public class TestController : ApiController
    {
        [Route("api/TriangleType")]
        public string GetTriangleType(int side1, int side2, int side3)
        {
            var values = new int[3] {side1, side2, side3};
            var result = string.Empty;
            if (side1 > 0 && side2 > 0 && side3 > 0)
                switch (values.Distinct().Count())
                {
                    case 1:
                        return result = "Equilateral";
                    case 2:
                        return result = "Isosceles";
                    case 3:
                        return result = "Scalene";
                    default:
                        return result;
                }
            else
                // return TriangleType.Error;
                return result = "Error returning type of triangle";
        }
        /// <summary>
        /// This is a function which accepts string 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/StringReverse")]
        public string StringReverse(string input)
        {


            if (input == null) throw new ArgumentNullException(nameof(input));
            var punctuation = new char[] {' ', ',', '.', ':', '\t', '?', '!', '@', '#'};
            var inputAsChars = input.ToCharArray();
            var result = string.Empty;

            var temp = "";
            if (!inputAsChars.ToString().EndsWith(" "))
            {
                StringBuilder sb = new StringBuilder();
                sb = sb.Append(input);
                sb = sb.Append(" ");

                inputAsChars = sb.ToString().ToCharArray();
            }
            foreach (var c in inputAsChars)
            {
                if (punctuation.Contains(c))
                {

                    char[] tx = temp.ToCharArray();
                    Array.Reverse(tx);
                    temp = new string(tx);
                    result += temp + c;
                    temp = "";
                }
                else
                    temp += c;
            }
            return result;
        }

        [HttpGet]
        [Route("api/Fibbonacci")]
        public int Fibbonacci(int num)
        {
            var a = 0;
            var b = 1;
            var result = 1;



            for (uint i = 0; i < num; i++)
            {
                result = b + a;
                a = b;
                b = result;
            }
            return result;




        }

        
        
        [HttpGet]
        [Route("api/Token")]
        public OkNegotiatedContentResult<string> GenerateToken()
        {
           IHttpActionResult response;
           HttpResponseMessage responseMsg = new HttpResponseMessage();
           string token = generateToken("Readify");
           return Ok<string>(token);
        }
        [NonAction]
        private string generateToken(string username)
        {
            //Set issued at date
            DateTime issuedAt = DateTime.UtcNow;
            //set the time when it expires
            DateTime expires = DateTime.UtcNow.AddDays(7);

            
            var tokenHandler = new JwtSecurityTokenHandler();

            //create a identity and add claims to the user which we want to log in
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name, username)
            });

            const string sec = "H4sIAAAAAAAEAMVXW28TRxR2QjIkIeEO4U64Q0mMA+aS1FwcOzdIiLEToKQQxuuJM8161+zshhhValX1+tJWSFV5adWqT1V/QlGlSn3hiaqtEBJt1Ze+VFUf+kIfKrXnzK7H682FvGG0S3zOd86cOefMN8ehmlAo9B988H/8NNfC63zS6AknzELBNMIZpjkWt0vtbZeZJbhpnO4MR/Bfe1vC0W3HYqcN5tgW1dvbUk5W59oFVho1p5lx2nB0vR4dt83jLwyylMUNjRepvgxATRPUsadM1DVNCJ43KDrfHBvMMcMG4ZnpiYkeqk1zI9/HmZ6rW163fT6/8bIX3EntzgWWLnuVIHgaMYTGOnzVw6sevy7uHhHLJ9hskVul1lia56dsEYxxYyxt6myOOLRsWctrmZKwWQH86zrTbEisCPczg1lcCw9xYd/sHB/3IBkb8pRvbysIzbR0nq2UIrrUUmRPnqTHteMnOruORVnkVNf16897fUz6vW9jv569+ejdRoJ5Xw6vVViB17Ems5+99+cX42ORaGTs/nep5uTKt3668TQ6u/fIud873rGd7OeRhxd+fnrvyZ1jXcf7o7c+efx+04O/D31g9r/x/d3Igy+/OjNywun+MPuwqZFdfVz79mDkUcdHb9Z9mnXs1+8WtzZ/fe0v9kftqh+frB4/eO/fpn/uf3NmoGHdx+FWse/mb780bn9Yj23wrO7BuFtjY4JZg8lgkTdJOWQgqNgRyzhZoVk8O696TwxbDP1rFMsyWiqyIKYtNih8KJYLAjbELtLCHLNQTU1NqKahBnP/QzIUIg3wB4ngp5M0Yg26ovJbNEqa4Otq2KpvGdOqIStQrFCvurZ1mIfn3VB4HskEB/+iHvjjDmuYmHG9QnQNDY3NoM96Tw32mmy6Fu/847Om2Ws+shJemxKJnm54wkMmzaVMwW0+w5LUpmQVaDeUtb2z0Bc2S7OiadlkNboBcc9It3yDcZ4bZA2yiV+cgOw4BWapP8hagBxeFNIrW27UooaYBIN1YLB3UYMhbmDpyXpA7vcjk0cT4QQ1NKan2S2HCXvESnJRhG2QDYDdtxBWONkCtz0bshGgW4JQeOKaZjqGTVqDm0Z9RrpI0VIBmopswjPmh6Ti4MINJc3y0DWw0c0AOvQMUFzgjSF9bgH4tiB8yMzlWXmPWwGxK4gYpoZD9XixqHvnjmwD2O4gDJ40K5gzVFfxbQfcnjk4ynNeohRwBwAPzAHCUbATFstxu4/rlV3vnC81QY9twR6YBxTXORVk13xrZxi1tCkP70vhbgC3B8FjxRwwTQqOlGlQPclsyis52BO0SDPoUQZtgycGO3KS5x1LJvYy1R1G9oLFkUUtvNMLKxa4wL8E2RdshWojfF2CKsIxSTs6I/sXhw/TYhHOBxJsiuYZORDMUTXcZaABU+c5WiIHAbxZgeXSwxRKyQw8LOTQ3KWRIEQ46bgdBpUG8st5vPECwDuWDO8zrQI5HKx92UQyBJUMnLGpLUh7MBa8mKDfYc9Y7nBiihp5SIEQt2EJ0gHwsB8+UmRu5dwVIGVhbNUeaqj6h4Nc4JFAGDobgidHguxWnVr1DWuBlwWJBNu/7NCf5M6FVnUbmxwNMlBZn2bcEDYGdixYc7jtixaDK08A3fdQW5s6b2Yxj44g0WDKJaPJteAC45MlNRcc91ZWF0iVW3mLnMBxByGDhaJ7qCCck5X7wy8+VRH3MxvLJ110gXhllViQbpCtxUu7G/ia2aqqL4J8nZQHyh3DOFyFrBW6IaeVMMl05gnPKKHLBVJ4Fs+BFEII5RnpEhKKPLHnsERSnaEzbK4+DvqtUh83xG1mBRGkB+m87N+dsqH9/TydUC4QgfN2AJDEKa0MKO87ZZmT0MSkF6/6BZSC9OFVL7XAWlwSYDlv/Sqh8VxOBjZqyowMKI/ypmBS12eZBakdVPVBM4jWszqvgvSsQKWMLoByYzlIWecr3J6SjsmQCrFKB+ZkWKkqE2fvLBwvQS6qMJImE2gmFWQE5zu/N9loKZC2VK1BLmHvVdeFpJUMi+2Gl1EO3U5ypaN+h7JmZEyJpDHGfznQhVJ4BYTry6a+MgtyVWnQg78DXlL5cx35ddeUrtL/YtDocfRpMq6C95/Gl/0RZCgED8QoWev6fJ02ABrTKpEbaiGXe69wI2felrknE8rQ1aHQgAleKm+qI+YqK8WUaoqlRCKR0zZVs2jWN8TOnc40jzsXGpJyHgEvOiIx/HFfDZpnQJr05q1njEd5b/RcwnA05bH2kqYAvhg4OAO8AuAdforHPgA60JgQ5cF3WhGOW485rKEraijX0Tt1BdUClRq6k5QgRuWwQl+7VfLuHVM1QHXpXa8tO9bgrxj85UyKPnYpcAPUFv5uI7cwIndbVYqW2hX/A6Im2XQDEgAA";
            var now = DateTime.UtcNow;
            var securityKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.Default.GetBytes(sec));
            var signingCredentials = new Microsoft.IdentityModel.Tokens.SigningCredentials(securityKey, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256Signature);


            //create the jwt
            var token =
                (JwtSecurityToken)
                    tokenHandler.CreateJwtSecurityToken(issuer: "http://localhost:58830/", audience: "http://localhost:58830/",
                        subject: claimsIdentity, notBefore: issuedAt, expires: expires, signingCredentials: signingCredentials);
            var tokenString = tokenHandler.WriteToken(token);

            return tokenString;
        }
    }

}

  
