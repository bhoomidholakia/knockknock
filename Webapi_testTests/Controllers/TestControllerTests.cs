﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Webapi_test.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using System.Net.Http;

namespace Webapi_test.Controllers.Tests
{
    [TestClass()]
    public class TestControllerTests
    {
        [TestMethod()]
        public void StringReverseTest()
        {
            TestController testController = new TestController();
            testController.Request = new HttpRequestMessage
            {
                RequestUri = new Uri("http://localhost:58830/api/StringReverse")
            };
            string result = testController.StringReverse("I am testing");
            result.Should().BeEquivalentTo("I ma gnitset "); 

          //  Assert.Fail();
        }
        [TestMethod()]
        public void Fibbonacci()
        {
            TestController testController = new TestController();
            testController.Request = new HttpRequestMessage
            {
                RequestUri = new Uri("http://localhost:58830/api/Fibbonacci")
            };
            int result = testController.Fibbonacci(2);
            result.Should().Be(2);

            //  Assert.Fail();
        }
        [TestMethod()]
        public void TriangleType()
        {
            TestController testController = new TestController();
            testController.Request = new HttpRequestMessage
            {
                RequestUri = new Uri("http://localhost:58830/api/TriangleType")
            };
            string result = testController.GetTriangleType(4,4,4);
            result.Should().BeEquivalentTo("Equilateral");

            //  Assert.Fail();
        }
    }
}